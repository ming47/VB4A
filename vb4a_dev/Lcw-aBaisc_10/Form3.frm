VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Begin VB.Form Form3 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "事件编辑"
   ClientHeight    =   9030
   ClientLeft      =   6570
   ClientTop       =   2310
   ClientWidth     =   10455
   LinkTopic       =   "Form3"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9030
   ScaleWidth      =   10455
   StartUpPosition =   2  '屏幕中心
   Begin VB.CommandButton btnCan 
      Caption         =   "Cancel"
      Height          =   495
      Left            =   8760
      TabIndex        =   34
      Top             =   6960
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Frame Frame4 
      Height          =   1215
      Left            =   9240
      TabIndex        =   28
      Top             =   7680
      Width           =   1095
      Begin VB.CommandButton Command1 
         Caption         =   "保存"
         Height          =   855
         Left            =   120
         TabIndex        =   29
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame Frame3 
      Height          =   1215
      Left            =   5880
      TabIndex        =   24
      Top             =   7680
      Width           =   3255
      Begin VB.CommandButton Command15 
         Caption         =   "Command15"
         Height          =   375
         Left            =   2280
         TabIndex        =   27
         Top             =   720
         Width           =   855
      End
      Begin VB.ComboBox Combo2 
         Height          =   300
         Left            =   120
         TabIndex        =   26
         Text            =   "Combo2"
         Top             =   240
         Width           =   3015
      End
      Begin VB.CommandButton Command12 
         Caption         =   "Command12"
         Height          =   375
         Left            =   1440
         TabIndex        =   25
         Top             =   720
         Width           =   855
      End
   End
   Begin VB.Frame Frame2 
      Height          =   1215
      Left            =   120
      TabIndex        =   12
      Top             =   7680
      Width           =   5655
      Begin VB.CheckBox Check4 
         Caption         =   "Check4"
         Height          =   375
         Left            =   3960
         TabIndex        =   31
         Top             =   720
         Width           =   1095
      End
      Begin VB.CommandButton Command16 
         Caption         =   "插入窗体"
         Height          =   375
         Left            =   4440
         TabIndex        =   30
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Command13 
         Caption         =   "插入颜色"
         Height          =   375
         Left            =   2280
         TabIndex        =   22
         Top             =   720
         Width           =   1095
      End
      Begin VB.CommandButton Command9 
         Caption         =   "插入元素"
         Height          =   375
         Left            =   1200
         TabIndex        =   18
         Top             =   720
         Width           =   1095
      End
      Begin VB.CommandButton Command14 
         Caption         =   "局部变量"
         Height          =   375
         Left            =   120
         TabIndex        =   23
         Top             =   720
         Width           =   1095
      End
      Begin VB.CheckBox Check3 
         Caption         =   "接受Tab"
         Height          =   255
         Left            =   5880
         TabIndex        =   21
         Top             =   600
         Value           =   1  'Checked
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton Command10 
         Caption         =   "高亮"
         Height          =   375
         Left            =   5040
         TabIndex        =   20
         Top             =   240
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CheckBox Check1 
         Caption         =   "自动补全代码"
         Height          =   255
         Left            =   5880
         TabIndex        =   19
         Top             =   840
         Value           =   1  'Checked
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.CommandButton Command7 
         Caption         =   "插入代码"
         Height          =   375
         Left            =   3360
         TabIndex        =   17
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Command8 
         Caption         =   "插入图片"
         Height          =   375
         Left            =   2280
         TabIndex        =   16
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Command11 
         Caption         =   "插入媒体"
         Height          =   375
         Left            =   1200
         TabIndex        =   15
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Command3 
         Caption         =   "公共变量"
         Height          =   375
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Width           =   1095
      End
      Begin VB.CheckBox Check2 
         Caption         =   "自动高亮"
         Height          =   255
         Left            =   5640
         TabIndex        =   13
         Top             =   720
         Width           =   1095
      End
   End
   Begin VB.CommandButton Command6 
      Caption         =   "HighLight"
      Height          =   495
      Left            =   7920
      TabIndex        =   11
      Top             =   6960
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Frame Frame1 
      Caption         =   "字体大小"
      Height          =   735
      Left            =   7920
      TabIndex        =   7
      Top             =   6120
      Visible         =   0   'False
      Width           =   1095
      Begin VB.CommandButton Command4 
         Caption         =   "+"
         Height          =   375
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.CommandButton Command5 
         Caption         =   "-"
         Height          =   375
         Left            =   600
         TabIndex        =   8
         Top             =   240
         Visible         =   0   'False
         Width           =   375
      End
   End
   Begin VB.TextBox Text2 
      Height          =   1575
      Left            =   11160
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   6
      Top             =   600
      Width           =   1935
   End
   Begin VB.CommandButton Command2 
      Caption         =   "编辑"
      Height          =   375
      Left            =   4680
      TabIndex        =   2
      Top             =   120
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ComboBox Combo1 
      Height          =   300
      Left            =   7200
      TabIndex        =   0
      Top             =   120
      Width           =   3135
   End
   Begin RichTextLib.RichTextBox RichTextBox1 
      Height          =   735
      Left            =   120
      TabIndex        =   32
      Top             =   7800
      Visible         =   0   'False
      Width           =   10215
      _ExtentX        =   18018
      _ExtentY        =   1296
      _Version        =   393217
      Appearance      =   0
      TextRTF         =   $"Form3.frx":0000
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   3375
      Left            =   6120
      ScaleHeight     =   3345
      ScaleWidth      =   4185
      TabIndex        =   33
      Top             =   4200
      Visible         =   0   'False
      Width           =   4215
   End
   Begin RichTextLib.RichTextBox Text1 
      Height          =   6855
      Left            =   120
      TabIndex        =   10
      Top             =   720
      Width           =   10215
      _ExtentX        =   18018
      _ExtentY        =   12091
      _Version        =   393217
      Enabled         =   -1  'True
      ScrollBars      =   3
      Appearance      =   0
      TextRTF         =   $"Form3.frx":009D
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Height          =   180
      Left            =   240
      TabIndex        =   5
      Top             =   480
      Width           =   90
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      Caption         =   "——"
      Height          =   180
      Left            =   1440
      TabIndex        =   4
      Top             =   120
      Width           =   360
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      Caption         =   "控件类型："
      Height          =   180
      Left            =   240
      TabIndex        =   3
      Top             =   120
      Width           =   900
   End
   Begin VB.Label Label1 
      Caption         =   "可选事件："
      Height          =   255
      Left            =   6000
      TabIndex        =   1
      Top             =   120
      Width           =   1095
   End
   Begin VB.Menu file 
      Caption         =   "file"
      Visible         =   0   'False
      Begin VB.Menu save 
         Caption         =   "save"
         Shortcut        =   ^S
      End
   End
   Begin VB.Menu Edit 
      Caption         =   "Edit"
      Visible         =   0   'False
      Begin VB.Menu 复制 
         Caption         =   "复制"
      End
      Begin VB.Menu 剪切 
         Caption         =   "剪切"
      End
      Begin VB.Menu 粘贴 
         Caption         =   "粘贴"
      End
   End
   Begin VB.Menu popm 
      Caption         =   "popmenu"
      Begin VB.Menu popitem 
         Caption         =   "popitem"
         Index           =   0
      End
   End
End
Attribute VB_Name = "Form3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Function ShowWindow Lib "user32" (ByVal hwnd As Long, ByVal nCmdShow As Long) As Long
Const SW_HIDE = 0
'Private Declare Function GetScrollPos Lib "user32" (ByVal hwnd As Long, ByVal nBar As Long) As Long
'Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
'Private Declare Function SetScrollPos Lib "user32" (ByVal hwnd As Long, ByVal nBar As Long, ByVal nPos As Long, ByVal bRedraw As Long) As Long



Dim 上一次操作的位置
Dim 操作前的内容
Dim Shift值
Dim KeyCode值
Dim 编辑状态

Public IMCANCELED As Boolean
Public MaterialType     As String
Public Materialname     As String
Public MaterialCapt     As String
Public TempFname        As String
Dim Filelist(1 To 1000) As String
Dim Counter_File        As String
Dim FileExits           As Boolean
Public withmenu         As Boolean
Dim Menustr(1 To 10)    As String
Dim ACStr(1 To 10000)   As String
Public Linecounter      As Integer
Public fixStr           As String
Public TypeStr          As String
Public NameStr          As String
Public Str2add          As String
Public inNamestr        As String
Public inTypeStr        As String
Public insertLine       As Integer
Public LastPos As Integer
Public curText As String
Public oldText As String

Public oldCode As String

Public Titems           As Integer

Public inComp As Boolean

Dim Lang3(1 To 30)      As String

Private saveMode As Integer


Private Declare Function GetCaretPos Lib "user32" (lpPoint As POINTAPI) As Long

Private Type POINTAPI
    x As Long
    y As Long
End Type

Public Sub iniCH3()
    On Error GoTo ErrHand

    Lang3(1) = "保存完成"
    Lang3(2) = "请先添加菜单项"
    Lang3(3) = "添加过程"
    Lang3(4) = "过程名："
    Lang3(5) = "添加函数"
    Lang3(6) = "函数名："
    Lang3(7) = "控件名："
    Lang3(8) = " 文件："

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "iniCH3", vbExclamation
End Sub

Public Sub iniEN3()
    On Error GoTo ErrHand

    Lang3(1) = "Saved!"
    Lang3(2) = "Please add a menu item first!"
    Lang3(3) = "Add Sub"
    Lang3(4) = "Sub Name:"
    Lang3(5) = "Add Func"
    Lang3(6) = "Func Name:"
    Lang3(7) = "Name:"
    Lang3(8) = "File:"

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "iniEN3", vbExclamation
End Sub

Private Sub btnCan_Click()
IMCANCELED = True
End Sub

Private Sub Combo1_Click()

If oldCode <> "" And oldCode <> Text1.text Then

    curText = Combo1.text

    If MsgBox(Form1.CHorEN(Form1.CH, "代码已修改，是否保存？|Save edit?"), vbYesNo, "") = vbYes Then
    
        Combo1.text = oldText
        Command1_Click
        Combo1.text = curText
        Command2_Click
        
    End If

Else

Command2_Click

End If



End Sub

Private Sub Combo1_KeyDown(KeyCode As Integer, Shift As Integer)
Debug.Print Combo1.ListIndex
End Sub

Private Sub Combo2_GotFocus()
    On Error GoTo ErrHand

    If Form1.inProject = True Then
        Combo2.Clear
        
        If Dir(App.Path + "\" + Form1.Text6.text + "\filelist.asn") = "" Then Exit Sub
        
        Open App.Path + "\" + Form1.Text6.text + "\filelist.asn" For Input As #1
        If LOF(1) <> 0 Then
            Do While Not EOF(1)
                Line Input #1, tmpstr
                If tmpstr <> "" Then
                    If Val(Split(tmpstr, "|")(1)) = Form1.Combo3.ListIndex + 1 Then
                        Me.Combo2.AddItem Mid((Split(tmpstr, "|")(0)), 1, Len(Split(tmpstr, "|")(0)) - 4)
                    End If
                End If
            Loop
        End If
        Close #1
    End If

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Combo2_GotFocus", vbExclamation
End Sub

Private Sub Command1_Click()
    On Error GoTo ErrHand

    'App.Path + "\" + Form1.Text6.text + "\"
    'MsgBox App.Path + "\" + Form1.Text6.text + "\"

    If saveMode = 1 Then
        
        If Me.Combo1.text = "" Or Me.Text1.text = "" Then Exit Sub
        
        Dim Tmp As String
        Open App.Path + "\" + Form1.Text6.text + "\" + TempFname + Me.Combo1.text + ".tmp" For Random As #1
        Close #1
        Open App.Path + "\" + Form1.Text6.text + "\" + TempFname + Me.Combo1.text + ".tmp" For Output As #1
        Print #1, Me.Text1.text
        Close #1
        
        Form5.OnlineCheck App.Path + "\" + Form1.Text6.text + "\" + TempFname + Me.Combo1.text + ".tmp"
        Text1.text = GetAllText(App.Path + "\" + Form1.Text6.text + "\" + TempFname + Me.Combo1.text + ".tmp")
        Open App.Path + "\" + Form1.Text6.text + "\filelist.asn" For Random As #1
        Close #1
        
        Open App.Path + "\" + Form1.Text6.text + "\filelist.asn" For Input As #1
        
        Do While Not EOF(1)
            Line Input #1, Tmp
            
            If TempFname + Me.Combo1.text + ".tmp" & "|" & Trim(Str(Form1.Combo3.ListIndex + 1)) = Tmp Then FileExits = True
        Loop
        
        Close #1
        
        If FileExits = False Then
            Open App.Path + "\" + Form1.Text6.text + "\filelist.asn" For Append As #1
            Print #1, TempFname + Me.Combo1.text + ".tmp"; "|"; Trim(Str(Form1.Combo3.ListIndex + 1))
            Close #1
        End If
        
    Else
        
        If Me.Combo2.text = "" Then Exit Sub
        
        Open App.Path + "\" + Form1.Text6.text + "\" + Combo2.text & ".tmp" For Output As #1
        Print #1, Me.Text1.text
        Close #1
        Form5.OnlineCheck App.Path + "\" + Form1.Text6.text + "\" + Combo2.text & ".tmp"
        Text1.text = GetAllText(App.Path + "\" + Form1.Text6.text + "\" + Combo2.text & ".tmp")
        
    End If
    
    'Call 装载代码
    Call Command10_Click
    MsgBox Lang3(1), vbOKOnly, ""
    oldCode = Text1.text
    
'    Call Command6_Click

    
    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command1_Click", vbExclamation
End Sub

Private Sub Command10_Click()
    On Error GoTo ErrHand
    Dim pos2 As Long
    pos = Text1.SelStart
    
    Dim CurPos As Long
    LockWindowUpdate Me.hwnd
    CurPos = GetCurrentPosition(Text1)

    Text1.SelStart = pos
    
    
    Set oSyntax = New CSyntax
    oSyntax.HighLightRichEdit Text1
    Set oSyntax = Nothing
    Text1.SelStart = pos

    SetScrollPos Me, Text1, CurPos, True
    LockWindowUpdate 0

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command10_Click", vbExclamation
End Sub

Private Sub Command11_Click()
    On Error GoTo ErrHand

    Dim pos As Long
    pos = Text1.SelStart
    AddMedia.Minpos = pos
    AddMedia.Show

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command11_Click", vbExclamation
End Sub

Private Sub Command12_Click()
    On Error GoTo ErrHand

    If Combo2.text = "" Then Exit Sub
    saveMode = 2
    Me.Text1.text = ""
    '多窗口的bug待修正哦
    Open App.Path + "\" + Form1.Text6.text + "\" + Combo2.text & ".tmp" For Input As #1
    
    Do While Not EOF(1)
        Line Input #1, Tmp
        Me.Text1.text = Me.Text1.text & Tmp & vbCrLf
    Loop
    
    Close #1

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command12_Click", vbExclamation
End Sub

Private Sub Command13_Click()
    On Error GoTo ErrHand

    colorPicker.Comefrom = 3
    colorPicker.Show

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command13_Click", vbExclamation
End Sub

Private Sub Command14_Click()
    On Error GoTo ErrHand

    Dim pos As Long
    pos = Text1.SelStart
    Form6.VAddmode = 1
    Form6.DefinePos = pos
    Form6.Caption = Form1.CHorEN(Form1.CH, "局部变量|Private Variant")
    Form6.Combo2.ListIndex = 0
    Form6.Show

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command14_Click", vbExclamation
End Sub

Private Sub Command15_Click()
    On Error GoTo ErrHand

    If Combo2.ListIndex = -1 Then Exit Sub
    If MsgBox(Form1.CHorEN(Form1.CH, "是否移除事件代码|Do you want to remove event src ") & Combo2.text & "?" & vbCrLf & Form1.CHorEN(Form1.CH, "事件将会删除！|Events will be deleted!"), vbYesNo + vbQuestion, "") = vbYes Then
        
        If Dir(App.Path + "\" + Form1.Text6.text + "\filelist.asn") = "" Then Exit Sub
        FileCopy App.Path + "\" + Form1.Text6.text + "\filelist.asn", App.Path + "\" + Form1.Text6.text + "\filelist.asn2"
        Open App.Path + "\" + Form1.Text6.text + "\filelist.asn2" For Input As #1
        Open App.Path + "\" + Form1.Text6.text + "\filelist.asn" For Output As #2
        
        If LOF(1) <> 0 Then
            Do While Not EOF(1)
                Line Input #1, tmpstr
                If tmpstr <> "" Then
                    If Split(tmpstr, "|")(0) = Combo2.text & ".tmp" Then
                        If Dir(App.Path + "\" + Form1.Text6.text + "\" + Split(tmpstr, "|")(0)) <> "" Then Kill App.Path + "\" + Form1.Text6.text + "\" + Split(tmpstr, "|")(0)
                    Else
                        Print #2, tmpstr
                    End If
                End If
            Loop
        End If
        Close #1
        Close #2
        If Dir(App.Path + "\" + Form1.Text6.text + "\filelist.asn2") <> "" Then Kill App.Path + "\" + Form1.Text6.text + "\filelist.asn2"
        
    End If
    

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command15_Click", vbExclamation
End Sub

Private Sub Command16_Click()

    Dim pos As Long
    pos = Text1.SelStart
    FormChooser.finpos = pos
    FormChooser.Show
    
End Sub

Private Sub DrawNotiText(inNoti As Variant)
Me.RichTextBox1.text = Form1.CHorEN(Form1.CH, Replace(inNoti, "_", "vbcrlf"))
End Sub

Private Sub Command2_Click()
    On Error GoTo ErrHand
    Me.RichTextBox1.text = ""
    Dim Tmp, temp2 As String
    Dim flst As String
    Dim codeString As String
    
    If Combo1.text = "" Then
        If Form1.CH = True Then
            MsgBox "请先选择事件再编辑！", vbCritical + vbOKOnly, ""
        Else
            MsgBox "Please choose an event first!", vbCritical + vbOKOnly, ""
        End If
        Exit Sub
    End If
    
    saveMode = 1
    
    Me.Text1.Enabled = True
    Me.Text1.SetFocus
    Me.Text1.text = ""
    
    'an ugly fix for form
    
    Debug.Print Me.MaterialType & Form1.Combo3.ListIndex
    
    If Me.MaterialType = "Form" And Form1.Combo3.ListIndex = 0 Then
        If Dir(App.Path + "\" + Form1.Text6.text + "\" + "Form" + Me.Combo1.text + ".tmp") <> "" Then
            
            
            Debug.Print "Fix Form"
            
            
            FileCopy App.Path + "\" + Form1.Text6.text + "\" + "Form" + Me.Combo1.text + ".tmp", App.Path + "\" + Form1.Text6.text + "\" + TempFname + Me.Combo1.text + ".tmp"
            
            Kill App.Path + "\" + Form1.Text6.text + "\" + "Form" + Me.Combo1.text + ".tmp"
            
            'then fix filelist.asn
            
            Open App.Path + "\" + Form1.Text6.text + "\filelist.asn" For Input As #1
            
            flst = ""
            
            If LOF(1) <> 0 Then
                
                Do While Not EOF(1)
                    Line Input #1, tmpstr
                    If tmpstr <> "" Then
                        flst = flst & tmpstr & vbCrLf
                    End If
                Loop
            End If
            
            Close #1
            
            flst = Replace(flst, "Form" + Me.Combo1.text + ".tmp", TempFname + Me.Combo1.text + ".tmp")
            
            Open App.Path + "\" + Form1.Text6.text + "\filelist.asn" For Output As #1
            Print #1, flst
            
            Close #1
        End If
    End If
    
    Open App.Path + "\" + Form1.Text6.text + "\" + TempFname + Me.Combo1.text + ".tmp" For Random As #1
    
    If LOF(1) = 0 Then
        Close #1
        
        Select Case Trim(Me.Combo1.text)
            
        Case "Initialize"
            
            DrawNotiText "Initialize事件，控件初始化时触发该事件。|Event Initialize, which will be raised when control initializes."
            
            
            If MaterialType = "Form" Then
                Me.Command3.Enabled = True
                Me.Text1.text = Me.Text1.text & "Event " & Materialname & "." & Me.Combo1.text & "()" & vbCrLf
                
                If withmenu = True Then
                    Open App.Path + "\" + Form1.Text6.text + "\menuitems.tmp" For Input As #2
                    
                    Do While Not EOF(2)
                        Line Input #2, temp2
                        Me.Text1.text = Me.Text1.text & "AddMenuItem(" & Chr(34) & temp2 & Chr(34) & ")" & vbCrLf
                    Loop
                    
                    Close #2
                End If
                
            Else
                Me.Text1.text = Me.Text1.text & "Event " & Materialname & "." & Me.Combo1.text & "()" & vbCrLf
            End If
            
        Case "Changed"
        
            DrawNotiText "Changed事件，控件值发生变化时触发此事件。|Event Changed, which will be raised when control value changes."
            
            If MaterialType = "LocationSensor" Then
                Me.Text1.text = Me.Text1.text & "Event " & Materialname & "." & Me.Combo1.text & "(latitude As Double, longitude As Double, altitude As Double)" & vbCrLf
            Else
                Me.Text1.text = Me.Text1.text & "Event " & Materialname & "." & Me.Combo1.text & "()" & vbCrLf
            End If
            
        Case "Keyboard"
        
            DrawNotiText "Keyboard事件，当设备按键按动时触发，keycode用于返回按键键值。|Event Keyboard, which will be raised when physical keys are pressed, and keycode will return the ascii of a certain key."
            
            Me.Text1.text = Me.Text1.text & "Event " & Materialname & "." & Me.Combo1.text & "(keycode As Integer)" & vbCrLf
            Me.Text1.text = Me.Text1.text & "Select keycode" & vbCrLf
            Me.Text1.text = Me.Text1.text & "Case Component.KEYCODE_PAD_CENTER" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "Case Component.KEYCODE_PAD_LEFT" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "Case Component.KEYCODE_PAD_RIGHT" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "Case Component.KEYCODE_PAD_UP" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "Case Component.KEYCODE_PAD_DOWN" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "Case Component.KEYCODE_HEADSETHOOK" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "Case Component.KEYCODE_CAMERA" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "Case Component.KEYCODE_VOLUME_DOWN" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "Case Component.KEYCODE_VOLUME_UP" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "Case Component.KEYCODE_SEARCH" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "Case Component.KEYCODE_BACK" & vbCrLf
            Me.Text1.text = Me.Text1.text & "Application.Quit()" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "End Select" & vbCrLf
            
        Case "MenuSelected"
            DrawNotiText "MenuSelected事件，当菜单选中时触发，caption用于标记所选择的菜单。|Event MenuSelected, which will be raised when a menu is clicked, caption will be used to mark the menu selected"
            If withmenu = False Then
                MsgBox Lang3(2), vbOKOnly, ""
                Exit Sub
            End If
            
            Me.Text1.text = Me.Text1.text & "Event " & Materialname & "." & Me.Combo1.text & "(caption As String)" & vbCrLf
            Dim ii, i As Integer
            Open App.Path + "\" + Form1.Text6.text + "\Menuitems.tmp" For Input As #1
            
            Do While Not EOF(1)
                ii = ii + 1
                Line Input #1, Menustr(ii)
            Loop
            
            Close #1
            Me.Text1.text = Me.Text1.text & "Select caption" & vbCrLf
            
            For i = 1 To Form1.Counter_Menu
                Me.Text1.text = Me.Text1.text & "Case " & Chr(34) & Menustr(i) & Chr(34) & vbCrLf
                Me.Text1.text = Me.Text1.text & vbCrLf
            Next i
            
            Me.Text1.text = Me.Text1.text & "End Select" & vbCrLf
            
        Case "TouchGesture"
        
            DrawNotiText Trim(Me.Combo1.text) & "事件，用于响应手势。|Event " & Trim(Me.Combo1.text) & ", which will be raised when certain gestures are presented on a form."
        
            Me.Text1.text = Me.Text1.text & "Event " & Materialname & "." & Me.Combo1.text & "(direction As Integer)" & vbCrLf
            Me.Text1.text = Me.Text1.text & "Select direction" & vbCrLf
            Me.Text1.text = Me.Text1.text & "Case Component.TOUCH_DOUBLETAP" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "Case Component.TOUCH_FLINGDOWN" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "Case Component.TOUCH_FLINGLEFT" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "Case Component.TOUCH_FLINGRIGHT" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "Case Component.TOUCH_FLINGUP" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "Case Component.TOUCH_MOVEDOWN" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "Case Component.TOUCH_MOVELEFT" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "Case Component.TOUCH_MOVEUP" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "Case Component.TOUCH_MOVERIGHT" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "Case Component.TOUCH_TAP" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "End Select" & vbCrLf
            
        Case "Declare"
        
        Case "ItemSelected"
            Me.Text1.text = Me.Text1.text & "Event " & Materialname & "." & Me.Combo1.text & "(index As Integer)" & vbCrLf
        
        Case "VB4AInputBoxClicked"
            DrawNotiText Trim(Me.Combo1.text) & "事件，用于响应输入框确定事件。|Event " & Trim(Me.Combo1.text) & ", which will be raised when yesbutton click of inputbox."
        
            Me.Text1.text = Me.Text1.text & "Event " & Materialname & "." & Me.Combo1.text & "(inpVal As String)" & vbCrLf
        
        Case "VB4AMsgboxClicked"
            Me.Text1.text = Me.Text1.text & "Event " & Materialname & "." & Me.Combo1.text & "(btn As Integer)" & vbCrLf
            Me.Text1.text = Me.Text1.text & "If btn=0 Then" & vbCrLf & "'btnYes" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "Else" & vbCrLf & "'btnNo" & vbCrLf & vbCrLf
            Me.Text1.text = Me.Text1.text & "End If"
            
        
        Case "Add_Sub"
            Me.Text1.text = Me.Text1.text & "Sub SUBNAME()" & vbCrLf
            Me.Text1.text = Me.Text1.text & vbCrLf
            Me.Text1.text = Me.Text1.text & "End Sub" & vbCrLf
            
        Case "Add_Function"
            Me.Text1.text = Me.Text1.text & "Function FUNCTIONNAME()" & vbCrLf
            Me.Text1.text = Me.Text1.text & vbCrLf
            Me.Text1.text = Me.Text1.text & "End Function" & vbCrLf
            
        Case "VB4ADown", "VB4AUp"
            DrawNotiText "VB4ADown或者VB4AUp事件，用于响应Canvas上的按下和抬起事件，返回触摸点的位置。|Event VB4AUp or VB4ADown, which will be raised when fingers pressed or released a canvas, returns the x-y value or the press or release point."
            Me.Text1.text = Me.Text1.text & "Event " & Materialname & "." & Me.Combo1.text & "(x As Integer, y As Integer)" & vbCrLf
            
        Case "VB4AMove"
            DrawNotiText "VB4AMove事件，用于响应Canvas上的手指移动事件，返回上一个坐标和当前坐标。|Event VB4AMove, which will be raised when fingers move around on a canvas."
            Me.Text1.text = Me.Text1.text & "Event " & Materialname & "." & Me.Combo1.text & "(lastx As Integer, lasty As Integer, currentx As Integer, currenty As Integer)" & vbCrLf
            
        
        Case "OrientationChanged"
            DrawNotiText Trim(Me.Combo1.text) & "事件，当重力感应器检测到放置方向改变时触发。|Event " & Trim(Me.Combo1.text) & ", which will be raised when Orientation was changed."
            Me.Text1.text = Me.Text1.text & "Event " & Materialname & "." & Me.Combo1.text & "(yaw As Single, pitch As Single, roll As Single)" & vbCrLf

        Case "LightChanged"
            DrawNotiText Trim(Me.Combo1.text) & "事件，当光线感应器检测到光照强度变化时触发。|Event " & Trim(Me.Combo1.text) & ", which will be raised when Orientation was changed."
            Me.Text1.text = Me.Text1.text & "Event " & Materialname & "." & Me.Combo1.text & "(light As Single)" & vbCrLf
            
        Case "TemperatureChanged"
            DrawNotiText Trim(Me.Combo1.text) & "事件，当温度传感器检测到温度改变时触发。|Event " & Trim(Me.Combo1.text) & ", which will be raised when Orientation was changed."
            Me.Text1.text = Me.Text1.text & "Event " & Materialname & "." & Me.Combo1.text & "(temperature As Single)" & vbCrLf
            
        Case "AccelerationChanged"
            DrawNotiText Trim(Me.Combo1.text) & "事件，设备加速度发生改变时触发。|Event " & Trim(Me.Combo1.text) & ", which will be raised when Acceleration of device was changed."
            Me.Text1.text = Me.Text1.text & "Event " & Materialname & "." & Me.Combo1.text & "(xAccel As Single, yAccel As Single, xAccel As Single)" & vbCrLf
            
        Case Lang3(3)
            
            Form7.addtype = "Sub"
            
            If Form1.CH = True Then
                
                Form7.Label1.Caption = Lang3(4)
            Else
                
                Form7.Label1.Caption = "Sub Name:"
            End If
            Form7.Combo2.Visible = False
            Form7.Label4.Visible = False
            
            
            Form7.Show
            
        Case Lang3(5)
            
            Form7.Label1.Caption = Lang3(6)
            Form7.addtype = "Function"
            Form7.Combo2.Visible = True
            Form7.Label4.Visible = True
            
            Form7.Show
            
        Case Else
        
            
        
        
        
            Me.Text1.text = Me.Text1.text & "Event " & Materialname & "." & Me.Combo1.text & "()" & vbCrLf
        End Select
        
        Me.Text1.text = Me.Text1.text & vbCrLf
        
        If Me.Combo1.text = Lang3(3) Or Me.Combo1.text = Lang3(5) Then
            Me.Text1.text = Me.Text1.text & vbCrLf
        Else
            Me.Text1.text = Me.Text1.text & "End Event" & vbCrLf
        End If
        
        'If Check2.Value = 1 Then Call Command6_Click
        
    Else
        
        If Me.Combo1.text = "Initialize" And MaterialType = "Form" Then Command3.Enabled = True
        
        Close #1
        Open App.Path + "\" + Form1.Text6.text + "\" + TempFname + Me.Combo1.text + ".tmp" For Input As #1
        
        codeString = ""
        
        Do While Not EOF(1)
            Line Input #1, Tmp
            codeString = codeString & Tmp & vbCrLf
        Loop
        
        Me.Text1.text = codeString
        
        Close #1
        
        Do Until right(Me.Text1.text, 2) <> vbCrLf
            
            Me.Text1.text = left(Me.Text1.text, Len(Me.Text1.text) - 2)
            
        Loop
        Me.Text1.text = Me.Text1.text & vbCrLf
        'Call Command6_Click
        
        Select Case Me.Combo1.text
            
        Case Lang3(3)
            
            Form7.addtype = "Sub"
            Form7.Label1.Caption = Lang3(4)
            Form7.Show
            
        Case Lang3(5)
            
            Form7.Label1.Caption = Lang3(6)
            Form7.addtype = "Function"
            Form7.Show
        End Select
        
    End If
    
    oldCode = Text1.text

IMCANCELED = False
'Call 装载代码
'Command6_Click
Command10_Click
    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command2_Click", vbExclamation
End Sub

Public Sub init()
    On Error GoTo ErrHand

    Dim tmpstram As String
    Me.Text1.Locked = False
    
    Me.Label2.Caption = Lang3(7) & Materialname & Lang3(8) & TempFname
    Me.Text1.text = ""
    Me.Combo1.Clear
    Me.Label8.Caption = MaterialType
    oldCode = ""
    Select Case MaterialType
        
    Case "Common"
        Me.Combo1.AddItem (Lang3(3))
        Me.Combo1.AddItem (Lang3(5))
        
    Case "Button"
        Me.Combo1.AddItem ("Initialize")
        Me.Combo1.AddItem ("Click")
        Me.Combo1.AddItem ("LongClick")
        Me.Combo1.AddItem ("GotFocus")
        Me.Combo1.AddItem ("LostFocus")
        
    Case "Image"
        Me.Combo1.AddItem ("Click")
        Me.Combo1.AddItem ("LongClick")
        
    Case "Label"
        Me.Combo1.AddItem ("Initialize")
        Me.Combo1.AddItem ("Click")
        Me.Combo1.AddItem ("LongClick")
        
    Case "LocationSensor"
        Me.Combo1.AddItem ("Initialize")
        Me.Combo1.AddItem ("Changed")
        
    Case "OrientationSensor"
        Me.Combo1.AddItem ("Initialize")
        Me.Combo1.AddItem ("OrientationChanged")
        
    Case "AccelerometerSensor"
        Me.Combo1.AddItem ("Initialize")
        Me.Combo1.AddItem ("AccelerationChanged")
        Me.Combo1.AddItem ("Shaking")
        
    Case "RadioButton"
        Me.Combo1.AddItem ("Initialize")
        Me.Combo1.AddItem ("Changed")
        Me.Combo1.AddItem ("GotFocus")
        Me.Combo1.AddItem ("LostFocus")
        
    Case "CheckBox"
        Me.Combo1.AddItem ("Initialize")
        Me.Combo1.AddItem ("Changed")
        Me.Combo1.AddItem ("GotFocus")
        Me.Combo1.AddItem ("LostFocus")
        
    Case "TextBox"
        Me.Combo1.AddItem ("Initialize")
        Me.Combo1.AddItem ("GotFocus")
        Me.Combo1.AddItem ("LostFocus")
        Me.Combo1.AddItem ("Validate(text as String, accept as Boolean)")
        
    Case "PasswordTextBox"
        Me.Combo1.AddItem ("Initialize")
        Me.Combo1.AddItem ("GotFocus")
        Me.Combo1.AddItem ("LostFocus")
        
    Case "Timer"
        Me.Combo1.AddItem ("Initialize")
        Me.Combo1.AddItem ("Timer")
        
    Case "Canvas"
        Me.Combo1.AddItem ("Initialize")
        Me.Combo1.AddItem ("VB4ADown")
        Me.Combo1.AddItem ("VB4AUp")
        Me.Combo1.AddItem ("VB4AMove")
    
    Case "VB4ASpinner"
        Me.Combo1.AddItem ("Initialize")
        Me.Combo1.AddItem ("GotFocus")
        Me.Combo1.AddItem ("LostFocus")
        Me.Combo1.AddItem ("ItemSelected")
        Me.Combo1.AddItem ("NothingSelected")
        
        
        
    Case "Phone", "Panel"
        Me.Combo1.AddItem ("Initialize")
        
    Case "Form"
        Me.Combo1.AddItem ("Initialize")
        Me.Combo1.AddItem ("Keyboard")
        Me.Combo1.AddItem ("VB4AInputBoxClicked")
        Me.Combo1.AddItem ("VB4AMsgboxClicked")
        Me.Combo1.AddItem ("MenuSelected")
        Me.Combo1.AddItem ("TouchGesture")
        
    Case "VB4ALSensor"
        Me.Combo1.AddItem ("LightChanged")
    
    
    Case "VB4ATSensor"
        Me.Combo1.AddItem ("TemperatureChanged")
        
    End Select
    

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "init", vbExclamation
End Sub

Private Sub Command3_Click()
    On Error GoTo ErrHand

    Form6.VAddmode = 0
    Form6.Caption = Form1.CHorEN(Form1.CH, "全局变量|Public Variant")
    Form6.Combo2.ListIndex = 0
    Form6.Show

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command3_Click", vbExclamation
End Sub

Private Sub Command4_Click()
    On Error GoTo ErrHand

    'Me.Text1.df = Me.Text1.FontSize + 1
    'If Me.Text1.FontSize > 14 Then Me.Text1.FontSize = 14
    

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command4_Click", vbExclamation
End Sub

Private Sub Command5_Click()
    On Error GoTo ErrHand

    'Me.Text1.FontSize = Me.Text1.FontSize - 1
    'If Me.Text1.FontSize < 8 Then Me.Text1.FontSize = 8
    

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command5_Click", vbExclamation
End Sub

Public Sub Command6_Click()
    On Error GoTo ErrHand

    
    If Check2.value = 1 Then
        pos = Text1.SelStart
        
        Dim CurPos As Long
        LockWindowUpdate Me.hwnd
        CurPos = GetCurrentPosition(Text1)
        SelectAll Me, Text1, True
        Text1.SelColor = vbBlack
        Text1.SelLength = 0
        SetScrollPos Me, Text1, CurPos, True
        LockWindowUpdate 0
        
        Text1.SelStart = pos
        
        HighLightComments Me, Text1, vbGreen, "'"
        KeyColor Me, Text1, vbBlue, B4Akeywords
        KeyColor Me, Text1, vbBlue, B4AkeywordsLcase
        
        If Dir(App.Path + "\" + Form1.Text6.text + "\TypeTable.tmp") <> "" Then KeyColor Me, Text1, vbCyan, getTypes
        StringColor Me, Text1, vbRed
        
        Text1.SelStart = pos
    End If
    

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command6_Click", vbExclamation
End Sub

Private Sub Command7_Click()
    On Error GoTo ErrHand

    Dim pos As Long
    pos = Text1.SelStart
    addinpa.inspos = pos
    addinpa.Show

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command7_Click", vbExclamation
End Sub

Private Sub Command8_Click()
    On Error GoTo ErrHand

    Dim pos As Long
    pos = Text1.SelStart
    inpic.Pinpos = pos
    inpic.Show

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command8_Click", vbExclamation
End Sub

Private Sub Command9_Click()
    On Error GoTo ErrHand

    Dim pos As Long
    pos = Text1.SelStart
    Insitem.ITinpos = pos
    Insitem.Show

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command9_Click", vbExclamation
End Sub

Private Sub Form_Load()
    On Error GoTo ErrHand

    Dim tmpstram As String
    FileExits = False
    
    If inProject = True Then
        
        Form1.SaveTypeTable
    End If
    
    Me.Command3.Enabled = False
    Call Form1.lan_changE(Form1.CH)
    
    Combo2.Clear
    popm.Visible = False
    
    oldCode = ""
    
    

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Form_Load", vbExclamation
End Sub

Private Sub Form_Unload(Cancel As Integer)
If oldCode <> "" And oldCode <> Text1.text Then

    curText = Combo1.text

    If MsgBox(Form1.CHorEN(Form1.CH, "代码已修改，是否保存？|Save edit?"), vbYesNo, "") = vbYes Then
    
        Combo1.text = oldText
        Command1_Click

        
    End If

End If
End Sub




Private Sub popitem_Click(Index As Integer)
    On Error GoTo ErrHand

    Str2add = popitem(Index).Caption
    refreshText2 (Form4.selpos)
    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "popitem_Click", vbExclamation
End Sub

Private Sub save_Click()
    On Error GoTo ErrHand

    'MsgBox

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "save_Click", vbExclamation
End Sub




''以下内容更换为新的代码

''see codebackup20140211.bas



''以上内容更换为新的代码


Public Function GetAllText(FileName As String) As String
    
    Dim sFile As String
    Open FileName For Input As #1
    sFile = StrConv(InputB$(LOF(1), #1), vbUnicode)
    Close #1
    GetAllText = sFile
    

    Exit Function
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Function: " & vbTab & "GetAllText", vbExclamation
End Function

Public Function getTypes() As String()
    On Error GoTo ErrHand

    Dim tmpstr1            As String, tmpstr2 As String
    Dim typestab           As Integer
    Dim tmptypes() As String
    
    
    typestab = 0
    If Dir(App.Path + "\" + Form1.Text6.text + "\TypeTable.tmp") <> "" Then
        Open App.Path + "\" + Form1.Text6.text + "\TypeTable.tmp" For Input As #33
        
        Do While Not EOF(33)
            Line Input #33, tmpstr1
            Line Input #33, tmpstr2
            
            If tmpstr1 <> "" And tmpstr2 <> "Empty" Then
                'tmptypes(typestab) = tmpstr1
                typestab = typestab + 1
            End If
            
        Loop
        
        Close #33
    
    ReDim tmptypes(typestab)
    
    typestab = 0
        Open App.Path + "\" + Form1.Text6.text + "\TypeTable.tmp" For Input As #33
        
        Do While Not EOF(33)
            Line Input #33, tmpstr1
            Line Input #33, tmpstr2
            
            If tmpstr1 <> "" And tmpstr2 <> "Empty" Then
                tmptypes(typestab) = tmpstr1
                typestab = typestab + 1
            End If
            
        Loop
        
        Close #33
        
        getTypes = tmptypes
    End If
    

    Exit Function
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Function: " & vbTab & "getTypes", vbExclamation
End Function

Function ExtString() As String
    On Error GoTo ErrHand

    Dim ShowCPF As Boolean
    
    Linecounter = 0
    Str2add = ""
    TypeStr = ""
    Dim meetspacE As Boolean
    meetspacE = False
    
    Dim tmpstrAC As String
    Dim tmpsave  As String
    Dim tmpsave2 As String
    
    For i = Text1.SelStart - 1 To 1 Step -1
        tmpstrAC = Mid(Text1.text, i, 1)
        
        If tmpstrAC = Chr(34) Or _
            tmpstrAC = " " Or _
            tmpstrAC = "=" Or _
            tmpstrAC = vbTab Or _
            tmpstrAC = "," Or _
            tmpstrAC = ":" Or _
            tmpstrAC = "(" Or _
            tmpstrAC = "+" Or _
            tmpstrAC = "-" Or _
            tmpstrAC = "*" Or _
            tmpstrAC = vbLf Or _
            tmpstrAC = "/" And _
            meetspacE = False Then
            meetspacE = True
            Exit For
        End If
        
        tmpsave2 = tmpstrAC & tmpsave2
    Next i
    
    '=========以上代码为寻找NameStr的过程
    
    'MsgBox tmpsave2
    
    ExtString = tmpsave2

    Exit Function
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Function: " & vbTab & "ExtString", vbExclamation
End Function

Sub AutoComplete()
    Dim ShowCPF As Boolean
    
    Linecounter = 0
    Str2add = ""
    TypeStr = ""
    Dim meetspacE As Boolean
    meetspacE = False
    
    
    
    Dim tmpstrAC As String
    Dim tmpsave  As String
    Dim tmpsave2 As String
    
    For i = Text1.SelStart - 1 To 1 Step -1
        tmpstrAC = Mid(Text1.text, i, 1)
        
        If tmpstrAC = Chr(34) Or _
            tmpstrAC = " " Or _
            tmpstrAC = "=" Or _
            tmpstrAC = vbTab Or _
            tmpstrAC = "," Or _
            tmpstrAC = ":" Or _
            tmpstrAC = "(" Or _
            tmpstrAC = "+" Or _
            tmpstrAC = "-" Or _
            tmpstrAC = "*" Or _
            tmpstrAC = vbLf Or _
            tmpstrAC = "/" And _
            meetspacE = False Then
            meetspacE = True
            Exit For
        End If
        
        tmpsave2 = tmpstrAC & tmpsave2
    Next i
    
    '=========以上代码为寻找NameStr的过程
    
    'MsgBox tmpsave2
    
    NameStr = tmpsave2
    
    Form1.SaveTypeTable
    Open App.Path + "\" + Form1.Text6.text + "\TypeTable.tmp" For Input As #1
    
    For i = 1 To Titems
        Line Input #1, inNamestr
        Line Input #1, inTypeStr
        
        If NameStr = inNamestr Then
            TypeStr = inTypeStr
            
        End If
        
    Next i
    
    'MsgBox TypeStr
    Close #1
    
    If TypeStr = "" Then
        
        Select Case NameStr
            
        Case "Application", "application"
            TypeStr = "Application"
            
        Case "Files", "files"
            TypeStr = "Files"
            
        Case "MyPhone", "myphone"
            TypeStr = "Phone"

        Case "VProp", "vprop"
            TypeStr = "VB4AProp"

        Case "MyTimer", "mytimer"
            TypeStr = "Timer"
            
        Case "GPS", "gps"
            TypeStr = "LocationSensor"
            
        Case "ORIENT", "orient"
            TypeStr = "OrientationSensor"
            
        Case "Acc", "acc"
            TypeStr = "AcceleromaterSenor"
            
        Case "LSensor", "lsensor"
            TypeStr = "VB4ALSensor"
            
        Case "TSensor", "tsensor"
            TypeStr = "VB4ATSensor"
            
        Case "VProp", "vprop"
            TypeStr = "VB4AProp"
            
        Case "VB4AInput", "vb4ainput"
            TypeStr = "VB4AInput"
            
        Case "Component", "component"
            TypeStr = "Component"
            
        Case Form1.Text6.text, LCase(Form1.Text6.text)
            TypeStr = "Form"
            
        Case "Math", "math"
            TypeStr = "Math"
            
        Case "Dates", "dates"
            TypeStr = "Dates"
            
        Case "Strings", "strings"
            TypeStr = "Strings"
            
        Case "Arrays", "arrays"
            TypeStr = "Arrays"
            
            
        Case "Conversions", "conversions"
            TypeStr = "Conversions"
            
        Case Else
            TypeStr = ""
            
        End Select
        
    End If
    
    
    ShowCPF = True
    Select Case TypeStr
        
        
        
    Case ""
        ShowCPF = False '避免小数输入是出现自动补全
        
    Case "Application"
        
        Form4.AddWithBefore = False
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("AddMenuItem(Name As String)")
        Form4.Combo1.AddItem ("Hide()")
        Form4.Combo1.AddItem ("Quit()")
        Form4.Combo1.AddItem ("SwitchForm(FormName As Form)")
        Form4.Combo1.AddItem ("GetPreference(Name As String)")
        Form4.Combo1.AddItem ("StorePreference(Name As String, Value as Variant)")
        Form4.Combo1.AddItem ("ToastMessage(msg As String)")
        Form4.Combo1.AddItem ("Msgbox(title As String, msg As String, btn As String)")
        Form4.Combo1.AddItem ("VB4AInputBoxShow(title As String, YesButton As String, NoButton As String)")
        Form4.Combo1.AddItem ("VB4AMsgboxShow(title As String, Msg As String, YesButton As String, NoButton As String)")
        Form4.Combo1.AddItem ("VB4AShell(cmd As String)")
        Form4.Combo1.AddItem ("Beep()")
        Form4.Combo1.AddItem ("GetTime()")
        Form4.Combo1.AddItem ("GetDate()")
        Form4.Combo1.AddItem ("PlayMedia(FilenameAsset As String)")
        Form4.Combo1.AddItem ("PlayMedia2(FilenameSD As String)")
        
        Form4.Combo1.AddItem ("CreatPlay(FilenameAsset As String)")
        Form4.Combo1.AddItem ("Play()")
        Form4.Combo1.AddItem ("StopPlay()")
        Form4.Combo1.AddItem ("PausePlay()")
        Form4.Combo1.AddItem ("SeekPlay(pos As Integer)")
        Form4.Combo1.AddItem ("ReleasePlay()")
        
        Form4.Combo1.AddItem ("GetFreeMem()")
        Form4.Combo1.AddItem ("GetTotMem()")
        
        Form4.Combo1.AddItem ("SQLEXEC(DBName As String, SQL_CMD As String)")
        Form4.Combo1.AddItem ("SQLPREPARE(DBName As String, SQL_CMD As String, ItemSeperator As String, LineSeperator As String)")
        
    
    Case "VB4AProp"
        Form4.AddWithBefore = False
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("GetIMEI()")
        Form4.Combo1.AddItem ("GetModel()")
        Form4.Combo1.AddItem ("GetSoftwareVersion()")
        Form4.Combo1.AddItem ("GetPhoneNum()")
        Form4.Combo1.AddItem ("GetSimSerial()")
        Form4.Combo1.AddItem ("GetID()")
    
    
    Case "VB4ASpinner"
        Form4.AddWithBefore = False
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("SetItem")
        Form4.Combo1.AddItem ("Enabled")
        Form4.Combo1.AddItem ("Value")
        Form4.Combo1.AddItem ("Index")
        
        
    Case "Arrays"
        Form4.AddWithBefore = False
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("Filter(array As String(), str As String, include As Boolean)")
        Form4.Combo1.AddItem ("Join(array As String(), separator As String)")
        Form4.Combo1.AddItem ("Split(str As String, separator As String, count As Integer)")
        Form4.Combo1.AddItem ("UBound(array As Variant,dim As Integer)")
        Form4.Combo1.AddItem ("Sort(array As Double,left As Integer, Right As Integer)")
        
    Case "VB4AInput"
        Form4.AddWithBefore = False
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("TEXTS")
        Form4.Combo1.AddItem ("NUMBER")
        Form4.Combo1.AddItem ("DATETIME")
        Form4.Combo1.AddItem ("MULTILINE")
        Form4.Combo1.AddItem ("PHONENUMBER")
        Form4.Combo1.AddItem ("NULL")
        
    Case "Files"
        
        Form4.AddWithBefore = False
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("Open(Name As String)")
        Form4.Combo1.AddItem ("Open2(NameSD As String)")
        Form4.Combo1.AddItem ("Close(Handle As Integer)")
        Form4.Combo1.AddItem ("Eof(Handle As Integer)")
        Form4.Combo1.AddItem ("WriteString(Handle As Integer, value As String)")
        Form4.Combo1.AddItem ("ReadString(Handle As Integer, value As String)")
        Form4.Combo1.AddItem ("WriteLong(Handle As Integer, value As Long)")
        Form4.Combo1.AddItem ("ReadLong(Handle As Integer, value As Long)")
        
        Form4.Combo1.AddItem ("ReadBoolean(Handle As Integer)")
        Form4.Combo1.AddItem ("WriteBoolean(Handle As Integer, value As Boolean)")
        Form4.Combo1.AddItem ("ReadByte(Handle As Integer)")
        Form4.Combo1.AddItem ("WriteByte(Handle As Integer, value As Byte)")
        Form4.Combo1.AddItem ("ReadDouble(Handle As Integer)")
        Form4.Combo1.AddItem ("WriteDouble(Handle As Integer, value As Double)")
        Form4.Combo1.AddItem ("ReadInteger(Handle As Integer)")
        Form4.Combo1.AddItem ("WriteInteger(Handle As Integer, value As Integer)")
        Form4.Combo1.AddItem ("ReadShort(Handle As Integer)")
        Form4.Combo1.AddItem ("WriteShort(Handle As Integer, value As Short)")
        Form4.Combo1.AddItem ("ReadSingle(Handle As Integer)")
        Form4.Combo1.AddItem ("WriteSingle(Handle As Integer, value As Single)")
        
        
        Form4.Combo1.AddItem ("Rename(oldname As String, newname As String)")
        Form4.Combo1.AddItem ("Rename2(oldnamesd As String, newnamesd As String)")
        Form4.Combo1.AddItem ("Delete(name As String)")
        Form4.Combo1.AddItem ("Delete2(namesd As String)")
        Form4.Combo1.AddItem ("Exists(name As String)")
        Form4.Combo1.AddItem ("Exists2(namesd As String)")
        Form4.Combo1.AddItem ("IsDirectory(name As String)")
        Form4.Combo1.AddItem ("IsDirectory2(namesd As String)")
        Form4.Combo1.AddItem ("Mkdir(name As String)")
        Form4.Combo1.AddItem ("Mkdir2(namesd As String)")
        Form4.Combo1.AddItem ("Rmdir(name As String)")
        Form4.Combo1.AddItem ("Rmdir2(namesd As String)")
        Form4.Combo1.AddItem ("Seek(handle As Integer, offset As Long)")
        Form4.Combo1.AddItem ("Size(handle As Integer)")
        Form4.Combo1.AddItem ("Unpack(assetFname As String, outPathAndFname As String")
        Form4.Combo1.AddItem ("GetSDPath()")
        Form4.Combo1.AddItem ("ReadTxt(FilePath As String)")
        Form4.Combo1.AddItem ("WriteTxt(FilePath As String,TxtToWrite As String)")
        
        
    Case "Math"
        
        Form4.AddWithBefore = False
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("E")
        Form4.Combo1.AddItem ("PI")
        
        Form4.Combo1.AddItem ("Abs(v)")
        Form4.Combo1.AddItem ("Atn(v)")
        Form4.Combo1.AddItem ("Atn2(y,x)")
        Form4.Combo1.AddItem ("Cos(v)")
        Form4.Combo1.AddItem ("Acos(v)")
        
        Form4.Combo1.AddItem ("Exp(v)")
        Form4.Combo1.AddItem ("Int(v)")
        Form4.Combo1.AddItem ("Log(v)")
        Form4.Combo1.AddItem ("Max(v1,v2)")
        Form4.Combo1.AddItem ("Min(v1,v2)")
        Form4.Combo1.AddItem ("Rnd()")
        Form4.Combo1.AddItem ("Sin(v)")
        Form4.Combo1.AddItem ("Asin(v)")
        Form4.Combo1.AddItem ("Ceil(v)")
        Form4.Combo1.AddItem ("Floor(v)")
        Form4.Combo1.AddItem ("Round(v)")
        Form4.Combo1.AddItem ("Ceil2(v,d)")
        Form4.Combo1.AddItem ("Floor2(v,d)")
        Form4.Combo1.AddItem ("Round2(v,d)")
        Form4.Combo1.AddItem ("Sgn(v)")
        Form4.Combo1.AddItem ("Sqr(v)")
        Form4.Combo1.AddItem ("Tan(v)")
        Form4.Combo1.AddItem ("DegreesToRadians(d)")
        Form4.Combo1.AddItem ("RadiansToDegrees(d)")
        
    Case "AcceleromaterSenor"
        
        Form4.AddWithBefore = True
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("Available")
        Form4.Combo1.AddItem ("Enabled")
        Form4.Combo1.AddItem ("XAccel")
        Form4.Combo1.AddItem ("YAccel")
        Form4.Combo1.AddItem ("ZAccel")
        
    Case "Button"
        
        Form4.AddWithBefore = True
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("BackgroundColor")
        Form4.Combo1.AddItem ("FontBold")
        Form4.Combo1.AddItem ("FontItalic")
        Form4.Combo1.AddItem ("FontSize")
        Form4.Combo1.AddItem ("Text")
        Form4.Combo1.AddItem ("Image")
        Form4.Combo1.AddItem ("TextColor")
        Form4.Combo1.AddItem ("Enabled")
        
    Case "Canvas"
        
        Form4.AddWithBefore = True
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("BackgroundColor")
        Form4.Combo1.AddItem ("BackgroundImage")
        Form4.Combo1.AddItem ("PaintColor")
        Form4.Combo1.AddItem ("Clear()")
        Form4.Combo1.AddItem ("DrawPoint(x, y)")
        Form4.Combo1.AddItem ("DrawCircle(x, y, r)")
        Form4.Combo1.AddItem ("DrawLine(x1, y1, x2, y2)")
        Form4.Combo1.AddItem ("VB4ADrawRect(x1, y1, x2, y2)")
        Form4.Combo1.AddItem ("VB4ADrawText(x As Integer, y As Integer, text As String)")
        Form4.Combo1.AddItem ("VB4ARotate(ang As Integer)")
        Form4.Combo1.AddItem ("VB4ADrawArc(x1,y1, x2, y2, angst, anged, ucenter)")
        Form4.Combo1.AddItem ("VB4ADrawRoundRect(x1, y1, x2, y2, rx, ry)")
        Form4.Combo1.AddItem ("VB4AInvalidate()")
        Form4.Combo1.AddItem ("SetVB4APaintSize(size As Integer)")
        
        
    Case "CheckBox"
        
        Form4.AddWithBefore = True
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("BackgroundColor")
        Form4.Combo1.AddItem ("FontBold")
        Form4.Combo1.AddItem ("FontItalic")
        Form4.Combo1.AddItem ("FontSize")
        Form4.Combo1.AddItem ("Text")
        Form4.Combo1.AddItem ("TextColor")
        Form4.Combo1.AddItem ("Enabled")
        Form4.Combo1.AddItem ("Value")
        
    Case "Form"
        
        Form4.AddWithBefore = True
        Form4.Combo1.Clear
        'Form4.Combo1.AddItem ("BackgroundColor")
        
        'Form4.Combo1.AddItem ("BackgroundImage")
        Form4.Combo1.AddItem ("Title")
        
    Case "Image"
        
        Form4.AddWithBefore = True
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("BackgroundColor")
        Form4.Combo1.AddItem ("Picture")
        
    Case "Panel"
        
        Form4.AddWithBefore = True
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("BackgroundColor")
        Form4.Combo1.AddItem ("Column")
        Form4.Combo1.AddItem ("Row")
        Form4.Combo1.AddItem ("Layout")
        
    Case "Label"
        
        Form4.AddWithBefore = True
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("BackgroundColor")
        Form4.Combo1.AddItem ("FontBold")
        Form4.Combo1.AddItem ("FontItalic")
        Form4.Combo1.AddItem ("FontSize")
        Form4.Combo1.AddItem ("Text")
        Form4.Combo1.AddItem ("TextColor")
        
    Case "LocationSensor"
        
        Form4.AddWithBefore = True
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("Available")
        Form4.Combo1.AddItem ("Enabled")
        Form4.Combo1.AddItem ("Latitude")
        Form4.Combo1.AddItem ("Longitude")
        Form4.Combo1.AddItem ("Altitude")
        Form4.Combo1.AddItem ("CurrentAddress")
        Form4.Combo1.AddItem ("Accuracy")
        
    Case "OrientationSensor"
        
        Form4.AddWithBefore = True
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("Available")
        Form4.Combo1.AddItem ("Enabled")
        Form4.Combo1.AddItem ("Yaw")
        Form4.Combo1.AddItem ("Pitch")
        Form4.Combo1.AddItem ("Roll")
        Form4.Combo1.AddItem ("Angle")
        Form4.Combo1.AddItem ("Magnitude")
        
    Case "VB4ALSensor"
        Form4.AddWithBefore = True
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("Available")
        Form4.Combo1.AddItem ("Temperature")
        Form4.Combo1.AddItem ("Enabled")
    
    Case "VB4ATSensor"
        Form4.AddWithBefore = True
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("Available")
        Form4.Combo1.AddItem ("Light")
        Form4.Combo1.AddItem ("Enabled")
    
    Case "PasswordTextBox"
        
        Form4.AddWithBefore = True
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("BackgroundColor")
        Form4.Combo1.AddItem ("FontBold")
        Form4.Combo1.AddItem ("FontItalic")
        Form4.Combo1.AddItem ("FontSize")
        Form4.Combo1.AddItem ("Text")
        Form4.Combo1.AddItem ("TextColor")
        Form4.Combo1.AddItem ("Hint")
        Form4.Combo1.AddItem ("Enabled")
        
    Case "Phone"
        
        Form4.AddWithBefore = True
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("Available")
        Form4.Combo1.AddItem ("Call(Phonenum as string)")
        Form4.Combo1.AddItem ("Vibrate(duration as integer)")
        Form4.Combo1.AddItem ("SendSMS(phonenum as String,text as String, warnings as String)")
        Form4.Combo1.AddItem ("SendMail(address as String,text as String)")
        Form4.Combo1.AddItem ("GetURL(url As String,codec As String) 'codec: GBK, UTF-8 or something alike")
        Form4.Combo1.AddItem ("JumpURL(url as String)")
        Form4.Combo1.AddItem ("SocketSend(ip As String, port As Integer, data As String)")
        
    Case "RadioButton"
        
        Form4.AddWithBefore = True
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("BackgroundColor")
        Form4.Combo1.AddItem ("FontBold")
        Form4.Combo1.AddItem ("FontItalic")
        Form4.Combo1.AddItem ("FontSize")
        Form4.Combo1.AddItem ("Text")
        Form4.Combo1.AddItem ("TextColor")
        Form4.Combo1.AddItem ("Enabled")
        Form4.Combo1.AddItem ("Value")
        
    Case "TextBox"
        
        Form4.AddWithBefore = True
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("BackgroundColor")
        Form4.Combo1.AddItem ("FontBold")
        Form4.Combo1.AddItem ("FontItalic")
        Form4.Combo1.AddItem ("FontSize")
        Form4.Combo1.AddItem ("InputType")
        Form4.Combo1.AddItem ("Singleline")
        Form4.Combo1.AddItem ("Text")
        Form4.Combo1.AddItem ("TextColor")
        Form4.Combo1.AddItem ("Hint")
        Form4.Combo1.AddItem ("Enabled")
        
    Case "PasswordTextBox"
        
        Form4.AddWithBefore = True
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("BackgroundColor")
        Form4.Combo1.AddItem ("FontBold")
        Form4.Combo1.AddItem ("FontItalic")
        Form4.Combo1.AddItem ("FontSize")
        Form4.Combo1.AddItem ("Text")
        Form4.Combo1.AddItem ("TextColor")
        Form4.Combo1.AddItem ("Hint")
        Form4.Combo1.AddItem ("Enabled")
        
    Case "Component"
        
        Form4.AddWithBefore = True
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("COLOR_NONE")
        Form4.Combo1.AddItem ("COLOR_BLACK")
        Form4.Combo1.AddItem ("COLOR_DKGRAY")
        Form4.Combo1.AddItem ("COLOR_GRAY")
        Form4.Combo1.AddItem ("COLOR_BLUE")
        Form4.Combo1.AddItem ("COLOR_CYAN")
        Form4.Combo1.AddItem ("COLOR_GREEN")
        Form4.Combo1.AddItem ("COLOR_LTGRAY")
        Form4.Combo1.AddItem ("COLOR_MAGENTA")
        Form4.Combo1.AddItem ("COLOR_RED")
        Form4.Combo1.AddItem ("COLOR_WHITE")
        Form4.Combo1.AddItem ("COLOR_YELLOW")
        
    Case "Dates"
        
        Form4.Combo1.Clear
        
        Form4.Combo1.AddItem ("DateAdd(date As Date, intervalKind As Integer, Interval As Integer)")
        Form4.Combo1.AddItem ("DateValue(value As String)")
        Form4.Combo1.AddItem ("Day(date As Date)")
        Form4.Combo1.AddItem ("FormatDate(date As Date)")
        Form4.Combo1.AddItem ("Hour(date As Date)")
        Form4.Combo1.AddItem ("Minute(date As Date)")
        Form4.Combo1.AddItem ("Month(date As Date)")
        Form4.Combo1.AddItem ("MonthName(date As Date)")
        Form4.Combo1.AddItem ("Now()")
        Form4.Combo1.AddItem ("Second(date As Date)")
        Form4.Combo1.AddItem ("Timer()")
        Form4.Combo1.AddItem ("Weekday(date As Date)")
        Form4.Combo1.AddItem ("WeekdayName(date As Date)")
        Form4.Combo1.AddItem ("Year(date As Date)")
        
        Form4.Combo1.AddItem ("DATE_YEAR")
        Form4.Combo1.AddItem ("DATE_MONTH")
        Form4.Combo1.AddItem ("DATE_DAY")
        Form4.Combo1.AddItem ("DATE_WEEK")
        Form4.Combo1.AddItem ("DATE_HOUR")
        Form4.Combo1.AddItem ("DATE_MINUTE")
        Form4.Combo1.AddItem ("DATE_SECOND")
        
        Form4.Combo1.AddItem ("DATE_JANUARY")
        Form4.Combo1.AddItem ("DATE_FEBRUARY")
        Form4.Combo1.AddItem ("DATE_MARCH")
        Form4.Combo1.AddItem ("DATE_APRIL")
        Form4.Combo1.AddItem ("DATE_MAY")
        Form4.Combo1.AddItem ("DATE_JUNE")
        Form4.Combo1.AddItem ("DATE_JULY")
        Form4.Combo1.AddItem ("DATE_AUGUST")
        Form4.Combo1.AddItem ("DATE_SEPTEMBER")
        Form4.Combo1.AddItem ("DATE_OCTOBER")
        Form4.Combo1.AddItem ("DATE_NOVEMBER")
        Form4.Combo1.AddItem ("DATE_DECEMBER")
        
        Form4.Combo1.AddItem ("DATE_MONDAY")
        Form4.Combo1.AddItem ("DATE_TUESDAY")
        Form4.Combo1.AddItem ("DATE_WEDNESDAY")
        Form4.Combo1.AddItem ("DATE_THURSDAY")
        Form4.Combo1.AddItem ("DATE_FRIDAY")
        Form4.Combo1.AddItem ("DATE_SATURDAY")
        Form4.Combo1.AddItem ("DATE_SUNDAY")
        
    Case "Strings"
        
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("InStr(str1 As String, str2 As String, start As Integer)")
        Form4.Combo1.AddItem ("InStrRev(str1 As String, str2 As String, start As Integer)")
        Form4.Combo1.AddItem ("LCase(ByRef str As String)")
        Form4.Combo1.AddItem ("UCase(ByRef str As String)")
        Form4.Combo1.AddItem ("Left(str As String, len As Integer)")
        Form4.Combo1.AddItem ("Right(str As String, len As Integer)")
        Form4.Combo1.AddItem ("Mid(str As String, start As Integer, len As Integer)")
        Form4.Combo1.AddItem ("Len(str As String)")
        Form4.Combo1.AddItem ("Trim(ByRef str As String)")
        Form4.Combo1.AddItem ("LTrim(ByRef str As String)")
        Form4.Combo1.AddItem ("RTrim(ByRef str As String)")
        Form4.Combo1.AddItem ("Replace(ByRef str As String, find As String, replace As String, start As Integer, count As Integer)")
        Form4.Combo1.AddItem ("StrComp(str1 As String, str2 As String)")
        Form4.Combo1.AddItem ("RC4(str As String, key As String)")
        Form4.Combo1.AddItem ("StrReverse(ByRef str As String)")
        Form4.Combo1.AddItem ("AscW(inChar As String)")
        Form4.Combo1.AddItem ("ChrW(unicodeVal As Long)")
        
        
    Case "Timer"
        
        Form4.AddWithBefore = True
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("Enabled")
        Form4.Combo1.AddItem ("Interval")
        
    Case "Conversions"
        
        Form4.AddWithBefore = True
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("Asc(str As String)")
        Form4.Combo1.AddItem ("Chr(value As Integer)")
        Form4.Combo1.AddItem ("Hex(v As Variant)")
        
    Case "VB4AProg"
        Form4.AddWithBefore = True
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("Value")
        Form4.Combo1.AddItem ("Max")
        
    Case "VB4AWeb"
        Form4.AddWithBefore = True
        Form4.Combo1.Clear
        Form4.Combo1.AddItem ("LoadURL(url As String)")
        Form4.Combo1.AddItem ("LoadURL2(url As String)")
        Form4.Combo1.AddItem ("GoBack()")
        Form4.Combo1.AddItem ("GoForward()")
        Form4.Combo1.AddItem ("Reload()")
        Form4.Combo1.AddItem ("Stop()")
        Form4.Combo1.AddItem ("SavePassword")
        Form4.Combo1.AddItem ("SaveFormData")
        Form4.Combo1.AddItem ("JSEnabled")
        Form4.Combo1.AddItem ("ZoomEnabled")
        Form4.Combo1.AddItem ("BuildinZoom")
        
    Case Else
        ShowCPF = False
    End Select
    
    pos = Text1.SelStart
    
    Form4.selpos = pos
    Debug.Print "autocomplete:["; NameStr; "->"; TypeStr, "], Show:"; ShowCPF
    
    
    If ShowCPF Then 'Form4.Show
        
        Dim C_XY As POINTAPI
        GetCaretPos C_XY
        Debug.Print C_XY.x, C_XY.y
        
        For i = 0 To popitem.Count - 1
            On Error Resume Next
            Unload popitem(i) '.Visible = False
        Next i
        
        For i = 0 To Form4.Combo1.ListCount - 1
            On Error Resume Next
            Form4.Combo1.ListIndex = i
            Load popitem(i)
            popitem(i).Visible = True
            popitem(i).Caption = Form4.Combo1.text
        Next i
        
        Me.PopupMenu popm, 0, C_XY.x * 15 + 15 * 15, C_XY.y * 15 + 50 * 15
        
        
        
    End If
    
End Sub


    

Public Sub refreshText()
    ACStr(insertLine) = ACStr(insertLine) & Str2add
    Me.Text1.text = ""
    
    For i = 1 To Linecounter
        Text1.text = Text1.text & ACStr(i) & vbCrLf
    Next i
    
    'Call Command6_Click

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "refreshText", vbExclamation
End Sub

Public Sub refreshText2(pos As Long)
    On Error GoTo ErrHand

    
    Dim str1 As String
    Dim str2 As String
    
    'pos = Text1.SelStart

    Text1.SelText = Str2add
    Text1.SelStart = pos + Len(Str2add)
    Text1.SetFocus
    'Call Command6_Click
    'Text1.text = Mid(Text1.text, 1, Text1.SelStart) & Str2add & Text1.text & Right(Text1.text, Len(Text1.text) - Text1.SelStart)

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "refreshText2", vbExclamation
End Sub

Private Sub Text1_Change()
'    Set oSyntax = New CSyntax
'    oSyntax.HighLightRichEdit Text1
'    Set oSyntax = Nothing
'    Text1.SelStart = pos
    oldText = Combo1.text
    Call Command10_Click
End Sub

Private Sub Text1_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ErrHand '

    If Check3.value = 1 Then
        If KeyCode = vbKeyTab Then
            Text1.SelText = Chr(vbKeyTab)
            Text1.SetFocus
            KeyCode = 0
        End If
    End If

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Text1_KeyDown", vbExclamation
End Sub

Private Sub Text1_Keyup(KeyCode As Integer, Shift As Integer)
    On Error GoTo ErrHand


    Dim pos As Long
    Dim StrEXT As String
    Dim StrTXT As String
    Dim POSTXT As Long
    Dim ifixStr As String
    If Check1.value = 0 Then Exit Sub
     inComp = False

    Debug.Print KeyCode


    If KeyCode = 190 Or KeyCode = 110 Then
        
        If Mid(Text1.text, Text1.SelStart, 1) = "." Then

            Call AutoComplete

            inComp = True

        End If

    End If

    'If KeyCode = 13 Then
        'Call Command6_Click
    'End If

    tmpstrAC = Chr(KeyCode)
    'Debug.Print KeyCode, tmpstrAC

    If tmpstrAC = Chr(34) Or _
       tmpstrAC = " " Or _
       KeyCode = 187 Or _
       tmpstrAC = vbTab Or _
       tmpstrAC = "," Or _
       tmpstrAC = ":" Or _
       tmpstrAC = "(" Or _
       tmpstrAC = "+" Or _
       tmpstrAC = "-" Or _
       tmpstrAC = "*" Or _
       tmpstrAC = vbLf Or _
       tmpstrAC = "/" Or _
       temstrac = "." Or _
       tmpstrAC = Chr(13) _
       Then

        If KeyCode = 187 Then ifixStr = "=" Else ifixStr = Chr(KeyCode)

        StrEXT = ExtString
        StrTXT = Text1.text
        POSTXT = Text1.SelStart

        Debug.Print "-->"; StrEXT

        '以下代码造成文本跳动
        '           If Len(LCase(StrEXT)) >= 5 Then
        '            If Mid(LCase(StrEXT), 1, 5) = "endif" Then
        '            Debug.Print "ENDIF SEPC"
        '                If KeyCode = 13 And Mid(Text1.text, Text1.SelStart - 1, 2) = vbNewLine Then
        '                 StrEXT = Replace(StrEXT, vbCr, "")
        '                 Debug.Print "Specially "; "["; StrEXT; "]", Len(StrEXT), inComp
        '                 Mid(StrTXT, Text1.SelStart - Len(StrEXT) - 1, Len(StrEXT) + 1) = Form5.Change(StrEXT)
        '                 Debug.Print "ENDIF VBCRLF"
        '                Else
        '                 Mid(StrTXT, Text1.SelStart - Len(StrEXT), Len(StrEXT) + 1) = Form5.Change(StrEXT) & ifixStr
        '                 Debug.Print "ENDIF SPC", "["; ifixStr; "]"
        '                End If
        '            End If
        '           Else
        '                If KeyCode = 13 And Mid(Text1.text, Text1.SelStart - 1, 2) = vbNewLine Then
        '                 StrEXT = Replace(StrEXT, vbCr, "")
        '                 Debug.Print "Specially "; "["; StrEXT; "]", Len(StrEXT), inComp
        '                 Mid(StrTXT, Text1.SelStart - Len(StrEXT) - 1, Len(StrEXT)) = Form5.Change(StrEXT)
        '                Else
        '                 Mid(StrTXT, Text1.SelStart - Len(StrEXT), Len(StrEXT)) = Form5.Change(StrEXT)
        '                End If
        '           End If
        '           Text1.text = StrTXT
        '以上代码造成文本跳动
        
        If Check4.value = 1 Then

            If Len(LCase(StrEXT)) >= 5 Then
                If Mid(LCase(StrEXT), 1, 5) = "endif" Then
    
                    Debug.Print "ENDIF SEPC"
                    If KeyCode = 13 And Mid(Text1.text, Text1.SelStart - 1, 2) = vbNewLine Then
                        StrEXT = Replace(StrEXT, vbCr, "")
                        Debug.Print "Specially "; "["; StrEXT; "]", Len(StrEXT), inComp
    
                        'Mid(StrTXT, Text1.SelStart - Len(StrEXT) - 1, Len(StrEXT) + 1) = Form5.Change(StrEXT)
    
                        Text1.SelStart = POSTXT - Len(StrEXT) - 2
                        Text1.SelLength = Len(StrEXT) + 1
                        Text1.SelText = Form5.Change(StrEXT)
    
                        Debug.Print "ENDIF VBCRLF"
                        Text1.SelStart = POSTXT + 1
                    Else
    
                        'Mid(StrTXT, Text1.SelStart - Len(StrEXT), Len(StrEXT) + 1) = Form5.Change(StrEXT) & ifixStr
    
                        Text1.SelStart = POSTXT - Len(StrEXT) - 1
                        Text1.SelLength = Len(StrEXT) + 1
                        Text1.SelText = Form5.Change(StrEXT) & ifixStr
    
                        Debug.Print "ENDIF SPC", "["; ifixStr; "]"
                        Text1.SelStart = POSTXT + 1
                    End If
                End If
    
            Else
    
                If KeyCode = 13 And Mid(Text1.text, Text1.SelStart - 1, 2) = vbNewLine Then
                    StrEXT = Replace(StrEXT, vbCr, "")
                    Debug.Print "Specially CRLF:"; "["; StrEXT; "]", Len(StrEXT), inComp
    
                    'Mid(StrTXT, Text1.SelStart - Len(StrEXT) - 1, Len(StrEXT)) = Form5.Change(StrEXT)
    
                    Text1.SelStart = POSTXT - Len(StrEXT) - 2
                    Text1.SelLength = Len(StrEXT)
                    Text1.SelText = Form5.Change(StrEXT)
    
                    Text1.SelStart = POSTXT
                Else
    
                    'Mid(StrTXT, Text1.SelStart - Len(StrEXT), Len(StrEXT)) = Form5.Change(StrEXT)
                    Debug.Print "NARMALFIX:"; "["; StrEXT; "]", Len(StrEXT)
                    Text1.SelStart = POSTXT - Len(StrEXT) - 1
                    Text1.SelLength = Len(StrEXT)
                    Text1.SelText = Form5.Change(StrEXT)
    
                    Text1.SelStart = POSTXT
                End If
    
            End If

        End If


        Text1.SelStart = POSTXT

        '==================
        'Text1.SelColor = vbBlack
        'If StrEXT <> Form5.Change(StrEXT) And Len(StrEXT) <> 0 Then
        'Debug.Print StrEXT; "<>"; Form5.Change(StrEXT)
        ' Text1.SelStart = POSTXT - Len(StrEXT) - 1
        ' Text1.SelLength = Len(StrEXT)
        ' Text1.SelColor = vbRed
        ' Debug.Print "sel:"; Text1.SelText
        ' Text1.SelStart = POSTXT
        'End If
        'Text1.SelColor = vbBlack
        '==================
        'Debug.Print "LastString：["; StrEXT; "] ChangedString: "; Form5.Change(StrEXT); " MID:["; Mid(StrTXT, Text1.SelStart - Len(StrEXT), Len(StrEXT)); "]", Len(StrEXT), Len(Mid(StrTXT, Text1.SelStart - Len(StrEXT), Len(StrEXT)))
    End If




    Exit Sub
ErrHand:
    Debug.Print "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Text1_Keyup", vbExclamation
End Sub


