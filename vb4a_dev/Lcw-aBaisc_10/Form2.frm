VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form Form2 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "生成"
   ClientHeight    =   7905
   ClientLeft      =   5445
   ClientTop       =   1440
   ClientWidth     =   8415
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7905
   ScaleWidth      =   8415
   StartUpPosition =   2  '屏幕中心
   Begin VB.CommandButton Command2 
      Caption         =   "Command2"
      Height          =   375
      Left            =   5760
      TabIndex        =   5
      Top             =   120
      Width           =   2535
   End
   Begin VB.ComboBox Combo1 
      Height          =   300
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   5535
   End
   Begin VB.TextBox Text2 
      Height          =   375
      Left            =   1440
      TabIndex        =   2
      Text            =   "0"
      Top             =   7440
      Width           =   1455
   End
   Begin RichTextLib.RichTextBox Text1 
      Height          =   6735
      Left            =   120
      TabIndex        =   1
      Top             =   600
      Width           =   8175
      _ExtentX        =   14420
      _ExtentY        =   11880
      _Version        =   393217
      ScrollBars      =   2
      Appearance      =   0
      AutoVerbMenu    =   -1  'True
      TextRTF         =   $"Form2.frx":0000
   End
   Begin VB.CommandButton Command1 
      Caption         =   "保存入工程"
      Height          =   375
      Left            =   5760
      TabIndex        =   0
      Top             =   7440
      Width           =   2535
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Label1"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   7440
      Width           =   1215
   End
End
Attribute VB_Name = "Form2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, _
ByVal wMsg As Long, _
ByVal wParam As Long, _
lParam As Any) As Long

Const EM_GETFIRSTVISIBLELINE = &HCE
Const EM_LINEFROMCHAR = &HC9
Const EM_GETLINECOUNT = &HBA
Const EM_LINEINDEX = &HBB

Public SRCName As String



Public Function TopLineIndex(txtBox As RichTextBox) As Long
    TopLineIndex = SendMessage(txtBox.hWnd, EM_GETFIRSTVISIBLELINE, 0&, 0&)

    Exit Function
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Function: " & vbTab & "TopLineIndex", vbExclamation
End Function

Public Function GetLineFromChar(txtBox As RichTextBox, CharPos As Long) As Long
    On Error GoTo ErrHand

    GetLineFromChar = SendMessage(txtBox.hWnd, EM_LINEFROMCHAR, CharPos, 0&)

    Exit Function
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Function: " & vbTab & "GetLineFromChar", vbExclamation
End Function

Public Function LineCount(txtBox As RichTextBox) As Long
    On Error GoTo ErrHand

    LineCount = SendMessage(txtBox.hWnd, EM_GETLINECOUNT, 0&, 0&)

    Exit Function
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Function: " & vbTab & "LineCount", vbExclamation
End Function

Public Function ColIndex(txtBox As RichTextBox, Lops As Long) As Long
    On Error GoTo ErrHand

    ColIndex = SendMessage(txtBox.hWnd, EM_LINEINDEX, Lops, 0)

    Exit Function
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Function: " & vbTab & "ColIndex", vbExclamation
End Function


    


Public Sub Command1_Click()
    'MsgBox App.Path + "\" + Form1.projectName + "\src\com\" + LCase(Form1.projectName) + "\" + Form1.projectName + ".simple"
    If SRCName = "" Then Exit Sub
    '    If Form1.Check1.Value = 1 Then
    '        Dim Chst2(1 To 10000) As String
    '        Dim Chcoun2           As Integer
    '        Chcoun2 = 0
    '
    '        fnmi = FreeFile()
    '        Open App.Path & "\lib\ch.dat" For Input As fnmi
    
    '    If LOF(fnmi) > 0 Then
    
    '            Do While Not EOF(fnmi)
    '                Chcoun2 = Chcoun2 + 1
    '                Line Input #fnmi, Chst2(Chcoun2)
    '            Loop
    
    '        End If
    
    '        Close fnmi
    
    '        For i = Chcoun2 To 1 Step -1
    '            Me.Text1.text = Replace(Me.Text1.text, Chst2(i), "Trans_" & Trim(str(i)))
    '        Next i
    
    '    End If
    
    Open App.Path + "\" + Form1.projectName + "\src\com\" + LCase(Form1.projectName) + "\" + SRCName + ".simple" For Output As #1
    Print #1, Me.Text1.text
    Print #1, "$Properties" & vbCrLf & "$Source $Form"
    Print #1, Form1.GUIs
    Close #1
    
    If Form1.Silently = False Then
        If Form1.CH = True Then
            MsgBox "修改已保存，请注意该保存操作仅作调试目的，如果能够运行请到对应的事件中修改代码！", vbOKOnly, ""
        Else
            MsgBox "Code saved for debug only, if your edit works for compilation, correspondding edits should be applied to events in code editor.", vbOKOnly, ""
        End If
    End If
    
    Me.Hide
    
    'Form5.Show
    Call Form5.execheck(SRCName)

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command1_Click", vbExclamation
End Sub



Private Sub Command2_Click()
    If Combo1.text = "" Then Exit Sub
    On Error Resume Next
    Text1.LoadFile (App.Path + "\" + Form1.projectName + "\src\com\" + LCase(Form1.projectName) + "\" + Combo1.text + ".simple")
    Dim tst() As String
    tst() = Split(Text1.text, "$Properties" & vbCrLf & "$Source $Form")
    Text1.text = tst(0)
    Form1.GUIs = tst(1)
    
End Sub

Private Sub Form_Load()
    On Error GoTo ErrHand

    Call Form1.lan_changE(Form1.CH)

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Form_Load", vbExclamation
End Sub


    


Private Sub Text1_SelChange()
    Dim lngLineCount As Long
    lngLineIndex = GetLineFromChar(Text1, Text1.SelStart)
    Text2.text = Trim(Str(lngLineIndex + 1))

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Text1_SelChange", vbExclamation
End Sub
