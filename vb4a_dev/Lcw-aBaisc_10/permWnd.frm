VERSION 5.00
Begin VB.Form permWnd 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   3690
   ClientLeft      =   6150
   ClientTop       =   1800
   ClientWidth     =   9615
   LinkTopic       =   "Form8"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3690
   ScaleWidth      =   9615
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Command2 
      Caption         =   "Command2"
      Height          =   375
      Left            =   1320
      TabIndex        =   3
      Top             =   3240
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   375
      Left            =   7800
      TabIndex        =   2
      Top             =   3240
      Width           =   1695
   End
   Begin VB.Frame Frame1 
      Caption         =   "Permissions"
      Height          =   3135
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   9375
      Begin VB.TextBox Text1 
         Height          =   2775
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   1
         Top             =   240
         Width           =   9135
      End
   End
End
Attribute VB_Name = "permWnd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public permSet As Boolean

Private Sub Command1_Click()
    On Error GoTo ErrHand

    
    Open App.Path & "\" & Form1.projectName & "\default.permfile" For Output As #42
    
    
    
    Print #42, Replace(Text1.text, vbCrLf & vbCrLf, vbCrLf)
    
    Close #42
    
    Form1.Permstr = Replace(Text1.text, vbCrLf & vbCrLf, vbCrLf)
    permSet = True
    Me.Hide

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command1_Click", vbExclamation
End Sub

Private Sub Command2_Click()
    On Error GoTo ErrHand

    Form1.Permstr = Replace(Text1.text, vbCrLf & vbCrLf, vbCrLf)
    permSet = True
    Me.Hide

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command2_Click", vbExclamation
End Sub

Private Sub Form_Load()
    On Error GoTo ErrHand

    If Form1.CH = True Then
        Frame1.Caption = "权限管理"
        Command1.Caption = "保存为默认"
        Command1.Caption = "保存"
    Else
        Frame1.Caption = "Permissions"
        Command1.Caption = "Save As Default"
        Command1.Caption = "Save"
    End If
    
    permSet = False

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Form_Load", vbExclamation
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo ErrHand

    permSet = True

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Form_Unload", vbExclamation
End Sub
