VERSION 5.00
Begin VB.Form AddMedia 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "插入音频"
   ClientHeight    =   1350
   ClientLeft      =   6300
   ClientTop       =   6975
   ClientWidth     =   4590
   LinkTopic       =   "Form8"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1350
   ScaleWidth      =   4590
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Command3 
      Caption         =   "关闭"
      Height          =   615
      Left            =   3000
      TabIndex        =   3
      Top             =   600
      Width           =   1455
   End
   Begin VB.CommandButton Command2 
      Caption         =   "插入"
      Height          =   615
      Left            =   1560
      TabIndex        =   2
      Top             =   600
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "播放"
      Height          =   615
      Left            =   120
      TabIndex        =   1
      Top             =   600
      Width           =   1455
   End
   Begin VB.ComboBox Combo1 
      Height          =   300
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4335
   End
End
Attribute VB_Name = "AddMedia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public MedEnable As Boolean
Public Minpos As Long

Private Music As clsMusic

Private Sub Command1_Click()
    On Error GoTo ErrHand

    If Combo1.ListIndex = -1 Or Combo1.text = "" Then
        If Form1.CH = True Then
            MsgBox "请选择一个媒体文件！", vbCritical + vbOKOnly, ""
        Else
            MsgBox "Choose a media file first!", vbCritical + vbOKOnly, ""
        End If
        
        Exit Sub
    End If
    
    Music.FileName = App.Path & "\" & Form1.Text6.text & "\assets\" & Combo1.text
    Music.Play
    

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command1_Click", vbExclamation
End Sub

Private Sub Command2_Click()
    On Error GoTo ErrHand

    Music.StopPlaying

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command2_Click", vbExclamation
End Sub

Private Sub Command3_Click()
    On Error GoTo ErrHand

    Form3.Str2add = Chr(34) & Trim(Combo1.text) & Chr(34)
    Call Form3.refreshText2(Minpos)
    Me.Hide

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command3_Click", vbExclamation
End Sub

Private Sub Form_Load()
    On Error GoTo ErrHand

    
    Set Music = New clsMusic
    'Music.Window = picTarget.hWnd
    
    
    If Form1.CH Then
        Command1.Caption = "播放"
        Command2.Caption = "停止"
        Command3.Caption = "插入"
        Me.Caption = "插入媒体"
    Else
        Command1.Caption = "Play"
        Command2.Caption = "Stop"
        Command3.Caption = "Insert"
        Me.Caption = "Insert Media"
    End If
    
    piclist = FreeFile()
    
    Dim MyFile As String, strFile As String
    
    Open App.Path & "\" & Form1.Text6.text & "\med.list" For Output As #piclist
    strFile = App.Path & "\" & Form1.Text6.text & "\assets\*.mp3"
    MyFile = Dir(strFile, vbNormal)
    
    Do While MyFile <> ""
        
        If InStr(1, MyFile, StrZf) > 0 Then Print #piclist, MyFile
        MyFile = Dir
    Loop
    
    strFile = App.Path & "\" & Form1.Text6.text & "\assets\*.wav"
    
    MyFile = Dir(strFile, vbNormal)
    
    Do While MyFile <> ""
        
        If InStr(1, MyFile, StrZf) > 0 Then Print #piclist, MyFile
        MyFile = Dir
    Loop
    
    strFile = App.Path & "\" & Form1.Text6.text & "\assets\*.mp4"
    MyFile = Dir(strFile, vbNormal)
    
    Do While MyFile <> ""
        
        If InStr(1, MyFile, StrZf) > 0 Then Print #piclist, MyFile
        MyFile = Dir
    Loop
    
    strFile = App.Path & "\" & Form1.Text6.text & "\assets\*.ogg"
    MyFile = Dir(strFile, vbNormal)
    
    Do While MyFile <> ""
        
        If InStr(1, MyFile, StrZf) > 0 Then Print #piclist, MyFile
        MyFile = Dir
    Loop
    
    strFile = App.Path & "\" & Form1.Text6.text & "\assets\*.mid"
    MyFile = Dir(strFile, vbNormal)
    
    Do While MyFile <> ""
        
        If InStr(1, MyFile, StrZf) > 0 Then Print #piclist, MyFile
        MyFile = Dir
    Loop
    
    Close piclist
    
    'If Dir(App.Path & "\" & Form1.Text6.text & "\pic.list") = "" Then
    '    If Form1.CH Then
    '    MsgBox "当前工程未发现图片 ！", vbCritical, ""
    '    Else
    '    MsgBox "No picture found in current project!", vbCritical, ""
    '    End If
    'insEnable = False
    
    'End If
    
    'Command1.Enabled = insEnable
    
    Combo1.Clear
    
    Open App.Path & "\" & Form1.Text6.text & "\med.list" For Input As #1
    
    If LOF(1) = 0 Then
        If Form1.CH Then
            MsgBox "当前工程未发现媒体！", vbCritical, ""
        Else
            MsgBox "No media file found in current project!", vbCritical, ""
        End If
        
        Close #1
        
        Me.Hide
        
    Else
        
        Do While Not EOF(1)
            
            Line Input #1, tmpstr
            Combo1.AddItem (tmpstr)
            
        Loop
        
        Close #1
    End If
    
    If MedEnable = False Then Me.Hide

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Form_Load", vbExclamation
End Sub
