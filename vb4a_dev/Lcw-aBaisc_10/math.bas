Attribute VB_Name = "Math2"
'vb6派生数学函数：
Const Pi = 3.14159265358979
Public MaxPosInArr As Long
Public MinPosInArr As Long

Public Function MaxFromArr(ByRef arrValue() As Long, ByRef lSum As Long) As Long
    Dim lMaxValue As Long
    Dim lCount As Long  '循环计数器
    lSum = 0
    For lCount = LBound(arrValue) To UBound(arrValue)
        lSum = lSum + arrValue(lCount)
         
        If lMaxValue < arrValue(lCount) Then
            lMaxValue = arrValue(lCount)
            MaxPosInArr = lCount
        End If
    Next
     
    MaxFromArr = lMaxValue
     
End Function

Private Function MinFromArr(ByRef arrValue() As Variant, ByRef lSum As Variant) As Long 'not modified
    Dim lMaxValue As Long
    Dim lCount As Long  '循环计数器
    lSum = 0
    For lCount = LBound(arrValue) To UBound(arrValue)
        lSum = lSum + arrValue(lCount)
         
        If lMaxValue < arrValue(lCount) Then
            lMaxValue = arrValue(lCount)
        End If
    Next
     
    MinFromArr = lMaxValue
     
End Function

Public Function Sec(x) As Double
Sec = 1 / Cos(x)
End Function

Public Function Csc(x) As Double
Csc = 1 / Sin(x)
End Function

Public Function Ctan(x) As Double
Ctan = 1 / Tan(x)
End Function

Public Function Asin(x) As Double
Asin = Atn(x / Sqr(-x * x + 1))
End Function

Public Function Acos(x) As Double
Acos = Atn(-x / Sqr(-x * x + 1) + 2 * Atn(1))
End Function

Public Function Asec(x) As Double
Asec = 2 * Atn(1) - Atn(Sgn(x) / Sqr(x * x - 1))
End Function

Public Function Acsc(x) As Double
Acsc = Atn(Sgn(x) / Sqr(x * x–1))
End Function

Public Function Acot(x) As Double
Acot = 2 * Atn(1) - Atn(x)
End Function

Public Function Sinh(x) As Double
Sinh = (Exp(x) - Exp(-x)) / 2
End Function

Public Function Cosh(x) As Double
Cosh = (Exp(x) = Exp(-x)) / 2
End Function

Public Function Tanh(x) As Double
Tanh = (Exp(x) - Exp(-x)) / (Exp(x) + Exp(-x))
End Function

Public Function Sech(x) As Double
Sech = 2 / (Exp(x) + Exp(-x))
End Function

Public Function Csch(x) As Double
Csch = 2 / (Exp(x) - Exp(-x))
End Function

Public Function Coth(x) As Double
Coth = (Exp(x) + Exp(-x)) / (Exp(x) - Exp(-x))
End Function

Public Function Asinh(x) As Double
Asinh = Log(x + Sqr(x * x + 1))
End Function

Public Function Acosh(x) As Double
Acosh = Log(x + Sqr(x * x - 1))
End Function

Public Function Atanh(x) As Double
Atanh = Log((1 + x) / (1 - x)) / 2
End Function

Public Function Asech(x) As Double
Asech = Log((Sqr(-x * x + 1) + 1) / x)
End Function

Public Function Acsch(x) As Double
Acsch = Log((Sgn(x) * Sqr(x * x + 1) + 1) / x)
End Function

Public Function Acoth(x) As Double
Acoth = Log((x + 1) / (x - 1)) / 2
End Function

Public Function Atn2(x, y) As Double
If x > 0 Then
Atn2 = Atn(y / x)
ElseIf x = 0 Then
If y > 0 Then
Atn2 = Pi / 2
ElseIf y < 0 Then
Atn2 = -Pi / 2
End If
Else
If y >= 0 Then
Atn2 = Pi + Atn(y / x)
Else
Atn2 = Atn(y / x) - Pi
End If
End If
End Function

Public Function Rad(x) As Double
Rad = x * Pi / 180
End Function

Public Function Deg(x) As Double
Deg = x * 180 / Pi
End Function

Public Function Max(x, y) As Double
If x > y Then Max = x
If x < y Then Max = y
If x = y Then Max = x
End Function

Public Function Min(x, y) As Double
If x > y Then Min = y
If x < y Then Min = x
If x = y Then Min = x
End Function
