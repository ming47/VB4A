VERSION 5.00
Begin VB.Form PanelProperty 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form8"
   ClientHeight    =   2625
   ClientLeft      =   10965
   ClientTop       =   5655
   ClientWidth     =   4215
   LinkTopic       =   "Form8"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2625
   ScaleWidth      =   4215
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox Combo2 
      Height          =   300
      Left            =   1200
      TabIndex        =   8
      Text            =   "Combo2"
      Top             =   2160
      Width           =   1335
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   495
      Left            =   2880
      TabIndex        =   6
      Top             =   2040
      Width           =   1215
   End
   Begin VB.TextBox Text2 
      Height          =   375
      Left            =   1200
      TabIndex        =   4
      Top             =   1560
      Width           =   975
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Left            =   1200
      TabIndex        =   3
      Top             =   960
      Width           =   975
   End
   Begin VB.ComboBox Combo1 
      Height          =   300
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   3975
   End
   Begin VB.Label Label4 
      Caption         =   "方向"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   2160
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "Label3"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   1680
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   1080
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "PanelProperty"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Combo1_Click()
If Combo1.ListIndex = 0 Then
    On Error GoTo ErrHand

Text1.Enabled = True
Text2.Enabled = True
Combo2.Enabled = False
Text1.text = Form1.Text8.text
Text2.text = Form1.Text9.text
ElseIf Combo1.ListIndex = 1 Then
Text1.Enabled = False
Text2.Enabled = False
Combo2.Enabled = True
Combo2.ListIndex = 0
Else
Text1.Enabled = False
Text2.Enabled = False
Combo2.Enabled = False
End If

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Combo1_Click", vbExclamation
End Sub

Private Sub Command1_Click()
    On Error GoTo ErrHand

Dim PType As String
Dim PColumn As String
Dim PRow As String
Dim POrie As String

Select Case Combo1.ListIndex

Case 0
    PType = "Component.LAYOUT_TABLE"
    PRow = Text1.text
    PColumn = Text2.text
Case 1
    PType = "Component.LAYOUT_LINEAR"
    
Case 2
    PType = "Component.LAYOUT_FRAME"
Case Else
    MsgBox Form1.CHorEN(Form1.CH, "类型选择错误！|Error in panel type!"), vbCritical + vbOKOnly, ""
    Exit Sub
End Select

Select Case Combo2.ListIndex
Case 0
    POrie = "Component.LAYOUT_ORIENTATION_HORIZONTAL"
Case 1
    POrie = "Component.LAYOUT_ORIENTATION_VERTICAL"
Case Else

End Select


Form1.InternalTC = PType
If PType = "Component.LAYOUT_TABLE" Then
Form1.InternalFJ = PRow & "|" & PColumn
ElseIf PType = "Component.LAYOUT_LINEAR" Then
Form1.InternalFJ = POrie
Else
Form1.InternalFJ = "Empty"
End If
        Form1.panperDefined = True

Me.Hide

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command1_Click", vbExclamation
End Sub

Private Sub Form_Load()
    On Error GoTo ErrHand

Me.Caption = Form1.CHorEN(Form1.CH, "Panel属性|Panel Property")
Me.Label1.Caption = Form1.CHorEN(Form1.CH, "Panel类型|Panel Layout Type")
Me.Label2.Caption = Form1.CHorEN(Form1.CH, "Panel行|Row")
Me.Label3.Caption = Form1.CHorEN(Form1.CH, "Panel列|Column")
Me.Label4.Caption = Form1.CHorEN(Form1.CH, "排列方向|Orientation")

Me.Command1.Caption = Form1.CHorEN(Form1.CH, "确定|Done")
Combo1.Clear
Combo1.AddItem (Form1.CHorEN(Form1.CH, "Table型|TableLayout"))
Combo1.AddItem (Form1.CHorEN(Form1.CH, "Linear型|LineLayout"))
Combo1.AddItem (Form1.CHorEN(Form1.CH, "Frame型|FrameLayout"))

Combo2.Clear
Combo2.AddItem (Form1.CHorEN(Form1.CH, "水平|Horizontal"))
Combo2.AddItem (Form1.CHorEN(Form1.CH, "垂直|Vertical"))

Text1.text = Form1.Text8.text
Text2.text = Form1.Text9.text



    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Form_Load", vbExclamation
End Sub
