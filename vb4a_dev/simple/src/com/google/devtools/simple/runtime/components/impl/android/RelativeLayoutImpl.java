/*
	LuChengwei 20140306 VB4A
 */

package com.google.devtools.simple.runtime.components.impl.android;

import com.google.devtools.simple.runtime.android.ApplicationImpl;
import com.google.devtools.simple.runtime.components.Component;
import com.google.devtools.simple.runtime.components.RelativeLayout;
import android.view.View;  
import android.view.ViewGroup.MarginLayoutParams;  

/**
 * Linear layout for placing components horizontally or vertically.
 *
 * @author Herbert Czymontek
 */
public final class RelativeLayoutImpl extends LayoutImpl implements RelativeLayout {

  /**
   * Creates a new linear layout.
   *
   * @param container  view container
   */
  RelativeLayoutImpl(ViewComponentContainer container) {
    super(new android.widget.RelativeLayout(ApplicationImpl.getContext()), container);

    android.widget.RelativeLayout layoutManager = (android.widget.RelativeLayout) getLayoutManager();
    //layoutManager.setWeightSum(1.0f);
    //layoutManager.setBaselineAligned(false);
    //Orientation(Component.LAYOUT_ORIENTATION_VERTICAL);
  }

  /*
  @Override
  public void Orientation(int newOrientation) {
    ((android.widget.RelativeLayout) getLayoutManager()).setOrientation(
        newOrientation == Component.LAYOUT_ORIENTATION_HORIZONTAL ?
            android.widget.RelativeLayout.HORIZONTAL :
            android.widget.RelativeLayout.VERTICAL);
  }
  */

  //关于布局的位置确定来自于：http://lovesong.blog.51cto.com/3976862/1183335
  //						  http://blog.csdn.net/tabactivity/article/details/9128271
  
  public static int dip2px(float dpValue) { 
	final float scale = ApplicationImpl.getContext().getResources().getDisplayMetrics().density; 
	return (int) (dpValue * scale + 0.5f); 
  } 
  
  public static int px2dip(float pxValue) { 
	final float scale = ApplicationImpl.getContext().getResources().getDisplayMetrics().density; 
	return (int) (pxValue / scale + 0.5f); 
  } 
  
    public static void setLayout(View view,int x,int y)  
    {  
        MarginLayoutParams margin=new MarginLayoutParams(view.getLayoutParams());  
        margin.setMargins(x,y, x+margin.width, y+margin.height);  
        android.widget.RelativeLayout.LayoutParams layoutParams = new android.widget.RelativeLayout.LayoutParams(margin);  
        view.setLayoutParams(layoutParams);  
    }  
  
  @Override
  public void addComponent(ViewComponent component) {
  
    int xp = component.Xpos();
    int yp = component.Ypos();
    int ht = component.Height();
	int wd = component.Width();
	View view = component.getView();
	
    getLayoutManager().addView(view, new android.widget.RelativeLayout.LayoutParams(
	android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT,
    android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT
	));
	
	setLayout(view,xp,yp);
	
//	MarginLayoutParams margin=new MarginLayoutParams(wd,ht);  
//	margin.setMargins(xp,yp, xp+wd, yp+ht);  
	/*
	MarginLayoutParams margin=new MarginLayoutParams(view.getLayoutParams());  
    margin.setMargins(xp,yp,0,0);  
    android.widget.RelativeLayout.LayoutParams layoutParams = new android.widget.RelativeLayout.LayoutParams(wd,ht); 
	getLayoutManager().addView(view);
	*/
	//view.setLayoutParams(layoutParams);
	/*
    android.widget.RelativeLayout.LayoutParams lp = new android.widget.RelativeLayout.LayoutParams(
	android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT,
    android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT
	);  
    //lp.addRule(android.widget.RelativeLayout.ALIGN_PARENT_TOP);  
    //lp.addRule(android.widget.RelativeLayout.ALIGN_PARENT_LEFT);  
	
	
	
	lp.addRule(android.widget.RelativeLayout.ALIGN_PARENT_LEFT, android.widget.RelativeLayout.TRUE); 
	lp.addRule(android.widget.RelativeLayout.ALIGN_PARENT_TOP, android.widget.RelativeLayout.TRUE); 
	lp.leftMargin=xp;
	lp.topMargin=yp;

    getLayoutManager().addView(component.getView(), lp);
	*/
  }
}
